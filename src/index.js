import * as faceapi from 'face-api.js';

/* Sélecteur */
const root = document.querySelector('#root');

/* Fonction principale de l'expérience */
async function init() {
    /* Charger le modèle */
    await faceapi.nets.ssdMobilenetv1.loadFromUri('/models');
    await faceapi.nets.faceLandmark68Net.loadFromUri('/models');
    await faceapi.nets.faceRecognitionNet.loadFromUri('/models');

    /* Créer le canvas */
    let canvas = document.createElement('canvas');
    let ctx = canvas.getContext('2d');
    root.appendChild(canvas);

    /* Dimension */
    canvas.style.width = '500px';
    canvas.style.height = '500px';

    /* Résolution */
    canvas.width = 800;
    canvas.height = 800;

    /* Ajouter un élément vidéo */
    let video = document.createElement('video');
    video.style.width = canvas.style.width;
    video.style.height = canvas.style.height;
    root.appendChild(video);

    /* Streamer la webcam (unsafely-treat-insecure-origin-as-secure) */
    navigator.getUserMedia(
        {
            video: true //{ width: 800, height: 800 }
        },
        stream => {
            video.srcObject = stream;
            video.play();
        },
        () => {}
    );

    /* Projeter sur le canvas */
    let faceBox = false;
    video.addEventListener('play', function() {
        let run = true;
        const renderFrame = async () => {
            /* Dessiner la vidéo */
            ctx.drawImage(video, 0, 0, 800, 800);

            /* Détecter le visage */
            detectFaces();

            /* Afficher */
            if (faceBox) {
                ctx.beginPath();
                ctx.fillStyle = '#FFAA00';
                ctx.lineWidth = 2;
                ctx.strokeStyle = '#FFAA00';
                ctx.rect(
                    faceBox._x,
                    faceBox._y,
                    faceBox._x + faceBox._width,
                    faceBox._y + faceBox._height
                );
                ctx.stroke();
                ctx.closePath();
            }

            /* Frame suivante */
            if (run) requestAnimationFrame(renderFrame);
        };
        renderFrame();

        /* Test */
        // setTimeout(() => {
        //     video.srcObject = null;
        //     run = false;
        // }, 30000);
    });

    /* Détecter les visages */
    const detectFaces = async () => {
        /* Extraire le résultat */
        const results = await faceapi
            .detectAllFaces(video)
            .withFaceLandmarks()
            .withFaceDescriptors();

        /* Retourner les infos utiles */
        faceBox = results.length ? results[0].detection.box : false;
    };
}

init();
